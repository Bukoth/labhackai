﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject storyPanel;
    public Text storyText;
    public GameObject keysPanel;
    public GameObject creditsPanel;

    private void Awake()
    {
        //Checks if the Panels are already active, if so calls a function to toggle it off.
        if (storyPanel.activeSelf == true)
        {
            ToggleStoryWindow();
        }

        if (keysPanel.activeSelf == true)
        {
            ToggleKeysWindow();
        }

        if (creditsPanel.activeSelf == true)
        {
            ToggleCreditsWindow();
        }
    }

    /// <summary>
    /// This function starts the Game by loading the game scene. 
    /// </summary>
    public void StartGame()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// Toggles Score panel between visible and not visible
    /// </summary>
    public void ToggleStoryWindow()
    {
        storyPanel.SetActive(!storyPanel.activeSelf);
        storyText.text = "Your name is April, and this is where your dad used to work. \n\nOn the first - floor labs.You used to visit his office as a child and spend the day there, looking through his books and learning science…. \n\nThere is no time for nostalgia! Your dad was ‘transferred’ to another branch.He has since disappeared, and your investigations have borne no fruit.\n\nOnly thing left to do is break into the Lab, go in his office, and download what you can from his computer in the hope that you may find some clues.\n\nYou must find him!You must find your dad! \n\nYou know there will be one night guard on duty. You have acquired a keycard that should work on the doors.\n\nYou must be careful and not get caught. \n\nYour only weapon is a tazer. Use it wisely and remember it needs time to rechage.";
        keysPanel.SetActive(false);
        creditsPanel.SetActive(false);
    }

    /// <summary>
    /// Toggles Key  panel between visible and not visible
    /// </summary>
    public void ToggleKeysWindow()
    {
        keysPanel.SetActive(!keysPanel.activeSelf);
        storyPanel.SetActive(false);
        creditsPanel.SetActive(false);
    }

    /// <summary>
    /// Toggles Credits panel between visible and not visible
    /// </summary>
    public void ToggleCreditsWindow()
    {
        creditsPanel.SetActive(!creditsPanel.activeSelf);
        storyPanel.SetActive(false);
        keysPanel.SetActive(false);
    }

    /// <summary>
    /// This function quits the whole Application. 
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }

}
