﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class GuardFSM : MonoBehaviour
{
    //GuardFSM determines the behaviour of the AI agent
    //
    // It has subclasses: 
    //                      GuardIdle   : FiniteStateMachine.BehaviourState
    //                      GuardPatrol : FiniteStateMachine.BehaviourState
    //                      GuardChase  : FiniteStateMachine.BehaviourState
    //                      GuardStun   : FiniteStateMachine.BehaviourState
    //                      GuardAttack : FiniteStateMachine.BehaviourState      
    //

    //How far the AI can 'see' to detect the player.
    public float viewRadius = 5.0f;
    
    //The following variables, each derived from its own class, appear in the inspector with drop down options. Neat!
    //In the Start() function below, an object will be initiated for each variable using the keyword 'new'
    public GuardIdle idleState;
    public GuardPatrol patrolState;
    public GuardChase chaseState;
    public GuardStun stunState;
    public GuardAttack attackState;

    //FSM Variables.  Once FSM class is created at the very bottom of this script, we need to keep a reference to it
    //Create an object from the FSM class.
    public FiniteStateMachine StateMachine { get; private set; } = new FiniteStateMachine();
    private bool targetInRange = false;

    public NavMeshAgent Agent { get; private set; }     //auto property represents AI Agent
    public Transform Target { get; private set; }       //auto property represents Player
    public Animator Anim { get; private set; }          //auto property represents the animations
    public AudioSource AudioSrc { get; private set; }   //auto property represents the audio clips

    private void Awake()
    {
        //Get the object references
        Agent = GetComponent<NavMeshAgent>();
        Anim = GetComponentInChildren<Animator>();
        AudioSrc = GetComponentInChildren<AudioSource>();     
    }

    /// <summary>
    /// On Start of the game, identify the target object "Player"
    /// Initiate all the AI states
    /// Set state to IDLE as the initial state
    /// </summary>
    private void Start()
    {
        Target = GameObject.FindWithTag("Player").transform;   

        idleState = new GuardIdle(this, idleState);
        patrolState = new GuardPatrol(this, patrolState);
        chaseState = new GuardChase(this, chaseState);
        stunState = new GuardStun(this, stunState);
        attackState = new GuardAttack(this, attackState);

        //Game start with AI in idle state
        StateMachine.SetState(idleState);  
    }

    private void Update()
    {
        if (Target != null)
        {
            //if distance to the target is less than the view radius then the target is in range
            if (Vector3.Distance(transform.position, Target.position) <= viewRadius)
            {
                targetInRange = true;
            }
            else
            {
                targetInRange = false;
            }

            //Conditions for switching to chase state: When in range or alerted
            if (StateMachine.CurrentState != chaseState && 
                StateMachine.CurrentState != stunState && 
                StateMachine.CurrentState != attackState)
            {
                if (GameController.instance.alerted == false && targetInRange == true)
                {
                    StateMachine.SetState(chaseState);
                }
                else if(GameController.instance.alerted == true) 
                {
                    StateMachine.SetState(chaseState);
                }
            }
        }

        //Update the state depending on the above conditions
        StateMachine.Update();
    }

    //This will not be removed until the Gold Master as testing and modifications are still taking place. 
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, viewRadius);
    }

    //------------------------------------------------------------------------------ GUARD IDLE ---------------------------------------------//
    [System.Serializable]
    public class GuardIdle : FiniteStateMachine.BehaviourState
    {
        //GuardIdle class determines the behaviour of the AI agent during the IDLE state and the state's exit conditions.
        //In IDLE, the AI agent will wait in position for a random amount of time.
        //An audio clip will play.
        //An animation will play.
        //State will exit to PATROL state when waiting time is up.

        public Vector2 waitRange;
        public AudioClip idleClip;

        private float time = 0;
        private float timer = -1;

        public GuardIdle(GuardFSM i, GuardIdle idleState) : base(i)
        {
            //Initialise variables
            ID = GuardStateID.idle;
            waitRange = idleState.waitRange;
            idleClip = idleState.idleClip;
        }

        public override void StateEnter()
        {
            timer = 0;
            time = Random.Range(waitRange.x,waitRange.y);

            //When Guard is in IDLE he doesn't move:
            instance.Agent.isStopped = true;

            //The following determine the transitions for the IDLE animation
            instance.Anim.SetBool("isChasing", false);
            instance.Anim.SetBool("isMoving", false);
            instance.Anim.SetBool("isStunned", false);

            //Audio clip plays at the start of the state once. 
            instance.AudioSrc.clip = idleClip;
            instance.AudioSrc.PlayOneShot(idleClip);
        }

        public override void StateUpdate()
        {
            //Increment the timer count and when it reaches the wait time transition to PATROL state
            if (timer >= 0)
            {
                timer += Time.deltaTime;
                if(timer >= time)
                {
                    timer = -1;                                                  //reset timer for next time.
                    instance.StateMachine.SetState(instance.patrolState);        //transition to patrol
                }
            }
        }
    }

    //------------------------------------------------------------------------------ GUARD PATROL ---------------------------------------------//
    [System.Serializable]
    public class GuardPatrol : FiniteStateMachine.BehaviourState
    {
        //GuardPatrol class determines the behaviour of the AI agent during the PATROL state and the state's exit conditions.
        //In PATROL, the AI agent will walk to a new position determined by the set waypoints in order.
        //An audio clip will play.
        //An animation will play.
        //State will exit to IDLE state when the destination waypoint is reached.

        public Transform[] patrolPoints;
        public float patrolSpeed = 3;
        public AudioClip patrolClip;

        private int currentIndex = -1;

        public GuardPatrol(GuardFSM i, GuardPatrol patrolState) : base(i)
        {
            //Initialise variables
            ID = GuardStateID.patrol;
            patrolPoints = patrolState.patrolPoints;
            patrolSpeed = patrolState.patrolSpeed;
            patrolClip = patrolState.patrolClip;
        }
     
        public override void StateEnter()
        {
            //When Guard is in PATROL he does move and is not stopped:
            instance.Agent.isStopped = false;

            //The following determine the transitions for the PATROL animation
            instance.Anim.SetBool("isChasing", false);
            instance.Anim.SetBool("isMoving", true);
            instance.Anim.SetBool("isStunned", false);

            //Audio clip plays at the start of the state once. 
            instance.AudioSrc.clip = patrolClip;
            instance.AudioSrc.PlayOneShot(patrolClip);

            GetNextIndex();
        }

        public override void StateUpdate()
        {
            //Vectors to ignore height of target
            Vector3 playerPos2D = new Vector3(instance.transform.position.x, 0, instance.transform.position.z);
            Vector3 targetPos2D = new Vector3(patrolPoints[currentIndex].position.x, 0, patrolPoints[currentIndex].position.z);

            float distance = Vector3.Distance(playerPos2D, targetPos2D);

            //If the agent is in range of the destination waypoint set to IDLE
            //Agent has reached the waypoint and will wait there.
            if (distance <= instance.Agent.stoppingDistance)
            {
                instance.StateMachine.SetState(instance.idleState);
            }
            else
            {
                //Keep playing the walking animation whilst walking
                if (instance.Agent.isStopped == true)
                {
                    instance.Agent.isStopped = false;
                    instance.Anim.SetBool("isMoving", true);
                }

                //If the agent has not reached the waypoint set agent's destination to the next waypoint in the list
                //This will move the agent towards that location.
                instance.Agent.SetDestination(patrolPoints[currentIndex].position);
            }
        }

        public override void StateExit()
        {
            instance.AudioSrc.Stop();
        }

        /// <summary>
        /// Increment the destination to the next waypoint in the list
        /// </summary>
        private void GetNextIndex()
        {
            currentIndex++;
            if(currentIndex >= patrolPoints.Length)
            {
                currentIndex = 0;
            }
        }
    }

    //------------------------------------------------------------------------------ GUARD CHASE ---------------------------------------------//

    [System.Serializable]
    public class GuardChase : FiniteStateMachine.BehaviourState
    {
        //GuardChase class determines the behaviour of the AI agent during the CHASE state and the state's exit conditions.
        //In CHASE, the AI agent will chase the player.
        //An audio clip will play.
        //An animation will play.
        //State will exit to PATROL if the player moves out of view range
        //State will exit to ATTACK if the player is in range of attack

        public float chaseSpeed = 5;
        public AudioClip chaseClip;
        public AudioClip chaseWarning;
        private float timer = 0;
        

        public GuardChase(GuardFSM i, GuardChase chaseState) : base(i)
        {
            //Initialise variables
            ID = GuardStateID.chase;
            chaseSpeed = chaseState.chaseSpeed;
            timer = chaseState.timer;
            chaseClip = chaseState.chaseClip;
            chaseWarning = chaseState.chaseWarning;
        }

        public override void StateEnter()
        {
            //When Guard is in CHASE he does move and is not stopped.
            instance.Agent.isStopped = false;

            //Audio clip plays at the start of the state once. 
            instance.AudioSrc.clip = chaseClip;
            instance.AudioSrc.PlayOneShot(chaseWarning);
            instance.AudioSrc.clip = chaseClip;
            instance.AudioSrc.PlayOneShot(chaseClip);

            //The following determine the transitions for the CHASE animation
            instance.Anim.SetBool("isChasing", true);
            instance.Anim.SetBool("isMoving", false);
            instance.Anim.SetBool("isStunned", false);

            instance.Agent.speed = chaseSpeed;
        }

        public override void StateUpdate()
        {
            //CHASE happens when Player is in range OR if alert is on
            if (instance.targetInRange == true || GameController.instance.alerted == true)
            {
                //Vectors to ignore height of target
                Vector3 playerPos2D = new Vector3(instance.transform.position.x, 0, instance.transform.position.z);
                Vector3 targetPos2D = new Vector3(instance.Target.position.x, 0, instance.Target.position.z);

                float distance = Vector3.Distance(playerPos2D, targetPos2D);

                //If Player is in range, initiate ATTACK state
                if (distance <= instance.Agent.stoppingDistance)
                {
                    instance.StateMachine.SetState(instance.attackState);
                }
                else
                {
                    //If player is not in range but still in view set the Destination to the Player. 
                    //Play chase animation throughout the state.
                    if (instance.Agent.isStopped == true)
                    {
                        instance.Agent.isStopped = false;

                        //The following determine the transitions for the CHASE animation
                        instance.Anim.SetBool("isMoving", false);
                        instance.Anim.SetBool("isChasing", true);
                        instance.Anim.SetBool("isStunned", false);
                    }

                    instance.Agent.SetDestination(instance.Target.position);
                }
            }
            //If alert is false go back to PATROL
            else if (GameController.instance.alerted == false)
            {
                instance.StateMachine.SetState(instance.patrolState);
            }
        }

        public override void StateExit()
        {
            //Turn off running sound
            instance.AudioSrc.Stop();

            //On state exit from CHASE check if the alert is on.
            //if not turn alert off
            //if so keep it red
            //if(GameController.instance.alerted == false)
            //{
            //    HUDManager.instance.UpdateChaseAlert(false);
            //}
            //else
            //{
            //    HUDManager.instance.UpdateChaseAlert(true);
            //}
        }
    }

    //------------------------------------------------------------------------------ GUARD STUN ---------------------------------------------//
    [System.Serializable]
    public class GuardStun : FiniteStateMachine.BehaviourState
    {
        //GuardStun class determines the behaviour of the AI agent during the STUN state and the state's exit conditions.
        //In STUN, the AI agent will freeze and wait in position for some amount of time.
        //An audio clip will play.
        //An animation will play.
        //State will exit to PATROL if Player is out of view.
        //State will exit to CHASE if Player is in view.

        public Vector2 stunRange;
        private float stunTime = 5;
        public AudioClip stunClip;

        private float timer = -1;

        public GuardStun(GuardFSM i, GuardStun stunState) : base(i)
        {
            //Initialise variables
            ID = GuardStateID.stun;
            stunTime = stunState.stunTime;
            stunClip = stunState.stunClip;
            stunRange = stunState.stunRange;
        }

        public override void StateEnter()
        {
            instance.Anim.transform.localPosition = Vector3.zero;

            timer = 0;
            stunTime = Random.Range(stunRange.x, stunRange.y);
            //When Guard is in STUN he doesn't move:
            instance.Agent.isStopped = true;

            //The following determine the transitions for the STUN animation
            instance.Anim.SetBool("isMoving", false);
            instance.Anim.SetBool("isChasing", false);
            instance.Anim.SetBool("isStunned", true);

            //Audio clip plays at the start of the state once. 
            instance.AudioSrc.clip = stunClip;
            instance.AudioSrc.PlayOneShot(stunClip);
        }

        public override void StateUpdate()
        {
            //Use timer to count up the chosen STUN duration.
            //Once time is up determine the next state depedning on teh conditions given below
            if (timer >= 0)
            {
                timer += Time.deltaTime;

                if(timer >= stunTime)
                {
                    timer = -1;

                    //Transition to relevant state
                    // not alerted and in range -> chase 
                    // not alerted not in range -> patrol
                    //     alerted              -> chase
                    if (GameController.instance.alerted == false)
                    {
                        if (instance.targetInRange == true)
                        {
                            //Initiate CHASE state
                            instance.StateMachine.SetState(instance.chaseState);
                        }
                        else
                        {
                            //Initiate PATROL state
                            instance.StateMachine.SetState(instance.patrolState);
                        }
                    }
                    else 
                    {
                        //Initiate CHASE state
                        instance.StateMachine.SetState(instance.chaseState);
                    }
                }
            }
        }

        public override void StateExit()
        {
            //Reset the position of the Agent as the stun animation was affecting his y-position at each stun
            //making him go into the floor when he gets up from his stun. 
            //This ensures he walks with his feet on the ground. 
            instance.Anim.transform.localPosition = Vector3.zero;
        }

    }

    //------------------------------------------------------------------------------ GUARD ATTACK ---------------------------------------------//

    [System.Serializable]
    public class GuardAttack : FiniteStateMachine.BehaviourState
    {
        //GuardAttack class determines the behaviour of the AI agent during the ATTACK state and the state's exit conditions.
        //In ATTACK, the AI agent will attack the player.
        //An audio clip will play.
        //An animation will play.
        //State will exit the game and a Game Over menu will be displayed.

        private float timer = 0;
        public float expireTime = 10;
        public AudioClip attackClip;
        public PauseMenu pauseMenu;

        public GuardAttack(GuardFSM i, GuardAttack attackState) : base(i)
        {
            //Initialise variables
            ID = GuardStateID.attack;
            expireTime = attackState.expireTime;
            attackClip = attackState.attackClip;
            pauseMenu = attackState.pauseMenu;
        }

        public override void StateEnter()
        {
            instance.transform.LookAt(FindObjectOfType<CharacterMovement>().transform);
            GameController.instance.attacked = true;
            //When Guard is in ATTACK he doesn't move:
            instance.Agent.isStopped = true;

            //Audio clip plays at the start of the state once. 
            instance.AudioSrc.clip = attackClip;
            instance.AudioSrc.PlayOneShot(attackClip);

            //The following determine the transitions for the ATTACK animation
            instance.Anim.SetBool("isMoving", false);
            instance.Anim.SetBool("isChasing", true);
            instance.Anim.SetBool("isStunned", false);
            instance.Anim.SetTrigger("Attack");

            //Disables the player so the player canno move or run away
            FindObjectOfType<CharacterController>().enabled = false;
            
        }

        public override void StateUpdate()
        {
            //Provide a bit of delay between the player being attacked and teh game ending
            //After the delay load Main Menu
            //Display cursor ready for the menu
            if (timer >= 0)
            {
                timer += Time.deltaTime;

                if (timer >= expireTime)
                {
                    timer = -1;

                    GameController.instance.gameOverState = true;
                    GameController.instance.gameWonState = false;
                    pauseMenu.GameOverMenu();

                }
            }
        }
    }
}
//End of GuardFSM:MonoBehaviour class -------------------------------------------------------------------------------------------------------------//


//------------------------------------------------------------------------------ Finite State Machine ---------------------------------------------//
//Creating a new class for the Finite State Machine
//The FSM determines the behaviour of the AI agent
//It has the nested sub class BehaviourState
public class FiniteStateMachine
{
    public BehaviourState CurrentState { get; private set; }    //auto-property for the current state

    /// <summary>
    /// Sets the state for the agent after exiting the current state. It takes the new state as a parameter and then makes the current state to be this new state. 
    /// </summary>
    /// <param name="newState"></param>
    public void SetState(BehaviourState newState)
    {
       if (CurrentState != null)
        {
            CurrentState.StateExit();      
        }

        CurrentState = newState;
        CurrentState.StateEnter();
    }

    public void Update()
    {
        //Run the corresponding StateUpdate() of the current state.
        if (CurrentState != null)
        {
            CurrentState.StateUpdate();
        }
    }

    //------------------------------------------------------------------------------ TEMPLATE CLASS: BehaviourState---------------------------------//
    /// <summary>
    /// Template class for defining new enemy states
    /// this is called a nested class it is nested under FSM
    /// </summary>
    public abstract class BehaviourState
    {
        public enum GuardStateID { idle, patrol, chase, stun, attack}       //Set the state IDs as a list

        protected GuardFSM instance;
        protected GuardStateID ID;

        //This is an empty constructor
        public BehaviourState(GuardFSM i)
        {
            instance = i;
        }

        public virtual void StateEnter(){ }         //Virtual function. The classes that inherit from this class will override this.
        public virtual void StateExit(){ }       
        public abstract void StateUpdate();         //Any function that inherits from BehaviourState has to use StateUpdate
    }
}