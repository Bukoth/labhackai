﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardSeeDoor : MonoBehaviour
{
    private float distance = 1.5f;


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        //Gizmos.DrawWireSphere(transform.position, 1f);
        //Vector3 sphereCentre = new Vector3(GetComponent<Transform>().position.x, GetComponent<Transform>().position.y + 0.75f, GetComponent<Transform>().position.z + distance);
        Gizmos.DrawWireSphere(transform.position, distance);
    }

    void Update()
    {
        //Vector3 guardEyes = new Vector3(GetComponent<Transform>().position.x, GetComponent<Transform>().position.y + 0.75f, GetComponent<Transform>().position.z);
        //Debug.Log(guardEyes);


        //Raycast door detection
        //Debug.DrawRay(guardEyes, GetComponent<Transform>().forward * distance, Color.magenta);

       //if (Physics.Raycast(guardEyes, GetComponent<Transform>().forward, out RaycastHit guardSees, distance) == true) //if raycast hits something
        if(Physics.SphereCast(transform.position, distance, transform.forward, out RaycastHit guardSees) == true)
        {
            Debug.Log("Guard sees " + guardSees);
            InteractableObject theThing = guardSees.transform.GetComponent<InteractableObject>();

            if (theThing == null)
            {
                //grab interaction reference from the object parent
                //attempt to reference interaction component from the parent object of the raycast hit object
                theThing = guardSees.transform.GetComponentInParent<InteractableObject>();
                Debug.Log("The things is " + theThing);
            }

            if (theThing is DoorInteraction door)
            {
                if(GetComponent<GuardFSM>().StateMachine.CurrentState == GetComponent<GuardFSM>().chaseState)
                {
                    door.OpenDoor();
                }
            }
        }
    }
}
