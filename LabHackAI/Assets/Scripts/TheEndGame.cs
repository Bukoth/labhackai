﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheEndGame : MonoBehaviour
{
    public GameController gameState;
    public GameController missionStatus;
    public PauseMenu pauseMenu;

    /// <summary>
    /// If the Fire Exit is reached and the mission is complete the game ends in success.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (missionStatus.missionComplete == true)
            {
                gameState.gameOverState = true;
                gameState.gameWonState = true;
                pauseMenu.GameWonMenu();
            }
        }
    }
}
