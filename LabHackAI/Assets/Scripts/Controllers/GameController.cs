﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public bool gameOverState;
    public bool gameWonState;
    public bool alerted;
    public bool missionComplete;
    public bool attacked;
    public bool stunUsed;
    public bool objective1Achieved;

    public GameObject staffroomDoor;

    public static GameController instance;  //wont show in inspector all instances will access singleton
                                            // Only want one Game Controller for the whole game.
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    //Start is called before the first frame update
    //Set Game Over t false. Game isn't over yet at the start.
    //Set Game Won to false. Game hasn't been won at the start yet.
    //alerted  and mission are both set to false 
    //Stun used up is set to false at the beginning
    void Start()
    {
        gameOverState = false;
        gameWonState = false;
        alerted = false;
        missionComplete = false;
        attacked = false;
        stunUsed = false;
        objective1Achieved = false;

        GetComponent<AudioSource>().volume = 0f;

        staffroomDoor.GetComponent<Animator>().SetBool("DoorOpen", true);
    }

    private void Update()
    {
        if(alerted == true)
        {
            //Ramp up the sound slowly when alerted chasing
            //timer += Time.deltaTime;       
            GetComponent<AudioSource>().volume += 0.01f;
        }
    }

    /// <summary>
    /// Play chase tune when the computer is hacked and alerted is true
    /// </summary>
    public void PlayChaseTune()
    {
        GetComponent<AudioSource>().Play();
        GetComponent<AudioSource>().loop = true;
    }
}
