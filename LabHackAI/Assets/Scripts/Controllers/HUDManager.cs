﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    //Alert Panel makes the whole screen go red when alerted is true
    public Image alertPanel;
    public Image stunCooldown;

    //Crosshair Information
    public Image crosshairActive;
    public Text helpText;
    public Image keycardImage;
    public Image downloadIcon;

    //This is the CyberDeck that shows the game objectives
    public GameObject objectivePanel;

    public Text alertedText;

    //Objective cursors on the CyberDeck
    public Image cursor1;
    public Image cursor2;
    public Image cursor3;

    //Crosshair instructions
    public Text instruction;
    public Image instructionBG;

    public static  HUDManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        instruction.enabled = false;
        instructionBG.enabled = false;
        alertPanel.enabled = false;

        keycardImage.enabled = false;
        downloadIcon.enabled = false;

        cursor1.enabled = true;
        cursor2.enabled = false;
        cursor3.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Objective") == true)
        {
            objectivePanelAnimate("toggle");
        }
    }

    /// <summary>
    /// Turns chase alert GUI element red of being chased/alerted
    /// and green if not.
    /// </summary>
    /// <param name="alertOn"></param>
    public void UpdateChaseAlert(bool alertOn)
    {
        if(alertOn == true)
        {    
            if (GameController.instance.alerted == true)
            {
                alertPanel.enabled = true;
                alertedText.color = Color.red;
            }
        }
        else
        {
            alertPanel.enabled = false;
        }    
    }

    /// <summary>
    /// Depletes Stun power to 0 when it is used.
    /// </summary>
    /// <param name="stunUsed"></param>
    public void StunUsedUp(bool stunUsed)
    {
        if (stunUsed == true)
        {
            stunCooldown.fillAmount = 0;
        }
    }

    public void updateCrossHair(Color colour, string word, bool visible)
    {
        instruction.text = word;
        instruction.color = Color.white;
        instructionBG.color = colour;
        crosshairActive.color = colour;

        if(word == "OPEN" || word == "SHUT")
        {
            keycardImage.enabled = true;
            downloadIcon.enabled = false;
            crosshairActive.enabled = false;
        }
        else if (word == "HACK")
        {
            keycardImage.enabled = false;
            crosshairActive.enabled = false;
            downloadIcon.enabled = true;
        }
        else if(word == "")
        {
            keycardImage.enabled = false;
            downloadIcon.enabled = false;
            crosshairActive.enabled = true;
        }

        if (visible == false)
        {
            instruction.enabled = false;
            instructionBG.enabled = false;
        }
        else 
        {
            instruction.enabled = true;
            instructionBG.enabled = true;
        }
    }

    /// <summary>
    /// When an objective is achieved HUD is updated. 
    /// </summary>
    public void updateObjectiveCursors()
    {
        if (GameController.instance.objective1Achieved == true)
        {
            cursor2.enabled = true;
            cursor1.GetComponent<Animator>().enabled = false;
            cursor1.color = Color.green;

            //when th eobjective done lower the panel if it is up
            objectivePanelAnimate("down");

            GameController.instance.objective1Achieved = false; //have to turn that off so the flow goes to ob2
        }
        else if(GameController.instance.missionComplete == true)
        {
            cursor3.enabled = true;
            cursor2.GetComponent<Animator>().enabled = false;
            cursor2.color = Color.green;

            //when th eobjective done lower the panel if it is up
            objectivePanelAnimate("down");
        }
    }


    /// <summary>
    /// Animates the HUD objective panel. Minimises and maximises.
    /// </summary>
    /// <param name="movement"></param>
    public void objectivePanelAnimate(string movement)
    {
        Animator targetAnim = objectivePanel.GetComponent<Animator>();
        bool toggle = targetAnim.GetBool("Panel");

        if (movement == "toggle")
        {   
            targetAnim.SetBool("Panel", !toggle);

            if (toggle == true)
            {
                helpText.text = "TAB to minimise.";
            }
            else
            {
                helpText.text = "TAB to maximise.";
            }
        }
        else if(movement == "down")
        {
            targetAnim.SetBool("Panel", false);
            helpText.text = "TAB to minimise.";
        }
    }
}
