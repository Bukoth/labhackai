﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    public float mouseSensitivity = 2.5f;
    public float mouseDrag = 1.5f;

    private Transform character;
    private Vector2 mouseDirection;
    private Vector2 smoothing;
    private Vector2 result;

    private void Awake()
    {
        character = transform.root;
    }

    // Update is called once per frame
    void Update()
    {
        //Fancy maths stuff that I will get my head around one day =)
        //Direction is constructed as a vector using the mouse x and y directions.
        //Smoothing is applied to the resuting vector that will be used for the camera rotation
        //Y rotation of the view is limited by angles to down and up angles
        mouseDirection = new Vector2(Input.GetAxisRaw("Mouse X") * mouseSensitivity, Input.GetAxisRaw("Mouse Y") * mouseSensitivity);
        smoothing = Vector2.Lerp(smoothing, mouseDirection, 1 / mouseDrag);
        result += smoothing;
        result.y = Mathf.Clamp(result.y, -60, 70);          //-20 degrees looking down 70 degrees looking up

        //angles are appied to the rotations
        transform.localRotation = Quaternion.AngleAxis(-result.y, Vector3.right);
        character.rotation = Quaternion.AngleAxis(result.x, character.up);
    }
}
