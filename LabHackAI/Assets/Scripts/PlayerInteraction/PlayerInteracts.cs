﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteracts : MonoBehaviour
{
    public float distance = 15f;
    public float stunDistance = 10f;

    void Update()
    {
        //------------------------------------------------------------------------------Crosshair Changing Colour --------------------------------------------/

        //This if statement toggles the crosshair between different colours
        //When crosshair hits an interactable object change the colour doors adn computer screen
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit crosshairHit, distance) == true) //if raycast hits something
        {
            InteractableObject theThing = crosshairHit.transform.GetComponent<InteractableObject>();

            if (theThing == null)
            {
                //grab interaction reference from the object parent
                //attempt to reference interaction component from the parent object of the raycast hit object
                theThing = crosshairHit.transform.GetComponentInParent<InteractableObject>();
            }

            //Check the object type and if it is a door and then change the crosshair instructions between OPEN and LOCKED
            //If the object is the computer to be hacked then change the wording to HACK with appropriate colours
            //if (crosshairHit.collider.GetComponent<InteractableObject>() == true || crosshairHit.collider.GetComponentInParent<InteractableObject>() == true)
            //{
            if (theThing != null)
            {
                if (theThing is DoorInteraction door)
                {
                    Color32 keycardBlue = new Color32(68, 114, 196, 255);

                    if (door.locked == false)
                    {
                        bool doorState = door.GetComponent<Animator>().GetBool("DoorOpen");

                        //If the door is shut display OPEN if it is open display SHUT
                        if (doorState == false)
                        {
                            HUDManager.instance.updateCrossHair(keycardBlue, "OPEN", true);
                        }
                        else
                        {
                            HUDManager.instance.updateCrossHair(keycardBlue, "SHUT", true);
                        }
                    }
                    else if (door.locked == true)
                    {
                        HUDManager.instance.updateCrossHair(keycardBlue, "OPEN", true);
                    }

                    //If the door is locked and the user tries to open door display LOCKED
                    if (Input.GetButton("Use") == true && door.locked == true)
                    {

                        HUDManager.instance.updateCrossHair(keycardBlue, "LOCKED", true);
                    }
                }
                else if (theThing is HackItStunIt)
                {
                    HUDManager.instance.updateCrossHair(Color.magenta, "HACK", true);
                }

                if (Input.GetButtonDown("Use") == true) 
                {
                    //if the ray hits an object
                    //if a reference to an interacble component has been made by this point
                    if (theThing != null)
                    {
                        //if computer is the object
                        theThing.Activate();
                    }
                }
            }
            else
            {
                HUDManager.instance.updateCrossHair(Color.green, "", false);
            }
        }
        else
        {
            HUDManager.instance.updateCrossHair(Color.green, "", false);
        }

        //When crosshair hits the guard change the colour 
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit enemyAimed, stunDistance) == true) //if raycast hits something
        {
            //Get guard info
            GuardFSM enemy = enemyAimed.transform.GetComponent<GuardFSM>();

            if (enemy != null)
            {
                if (enemy != null)
                {
                    HUDManager.instance.updateCrossHair(Color.red, "STUN", true);
                }
                else
                {
                    HUDManager.instance.updateCrossHair(Color.green, "", false);
                }

                if (Input.GetButtonDown("Use") == true)
                {
                    //if the stun is used do not allow for it to be used again.
                    //else call GUI function to deplete stun and set used to true so it can't be used again.
                    //Then set AI state to stun
                    if (GameController.instance.stunUsed == true)
                    {
                        //inlcude some audio with tazer not working
                    }
                    else if (GameController.instance.attacked == false)
                    {
                        HUDManager.instance.StunUsedUp(true);
                        GameController.instance.stunUsed = true;
                        enemy.StateMachine.SetState(enemy.stunState);
                    }
                }
            }
        }
    }
}
