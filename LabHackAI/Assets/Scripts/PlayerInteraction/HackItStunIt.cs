﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HackItStunIt : InteractableObject
{
    public GameController alert;
    public GameController mission;
    public Text downloadText;

    //Lab A,B and C alert lights
    public Light alarmLightA;
    public GameObject lightWhiteLabA;
    public GameObject lightRedLabA;

    public Light alarmLightB;
    public GameObject lightWhiteLabB;
    public GameObject lightRedLabB;

    public Light alarmLightC;
    public GameObject lightWhiteLabC;
    public GameObject lightRedLabC;


    public void Start()
    {
        //set the alarm lights all to false at the beginning
        downloadText.enabled = false;
        lightRedLabA.SetActive(false);
        lightRedLabB.SetActive(false);
        lightRedLabC.SetActive(false);
    }

    public override void Activate()
    {
        //Set off the download animation and bring up the downloading text on the screen
        GetComponentInChildren<Animator>().SetTrigger("Hacked");
        downloadText.enabled = true;

        float lightIntensity = 30f;

        //Turn on all the alarm colours to Red with high intensity
        alarmLightA.color = Color.red;
        alarmLightA.intensity = lightIntensity;

        alarmLightB.color = Color.red;
        alarmLightB.intensity = lightIntensity;

        alarmLightC.color = Color.red;
        alarmLightC.intensity = lightIntensity;

        //Swap white glow emission to red glow emission
        lightRedLabA.SetActive(true);
        lightWhiteLabA.SetActive(false);

        lightRedLabB.SetActive(true);
        lightWhiteLabB.SetActive(false);

        lightRedLabC.SetActive(true);
        lightWhiteLabC.SetActive(false);

        //Set off alerts, game objective GUI elements
        alert.alerted = true;
        mission.missionComplete = true;
        HUDManager.instance.updateObjectiveCursors();
        HUDManager.instance.UpdateChaseAlert(true);
        HUDManager.instance.updateCrossHair(Color.magenta, "HACK", true);
        GameController.instance.PlayChaseTune();
    }
}
