﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteraction : InteractableObject
{
    public bool locked = false;  //variable to test if door is locked or not

    public AudioClip lockedSound;
    public AudioClip openSound;
    public AudioClip shutSound;

    /// <summary>
    /// Plays appropriate door sounds. Parameters: "locked" "open" "shut"
    ///
    /// </summary>
    /// <param name="doorAction"></param>
    public void PlayDoorSound(string doorAction)
    {
        if(doorAction == "locked")
        {
            GetComponent<AudioSource>().PlayOneShot(lockedSound);
        }
        else if (doorAction == "open")
        {
            GetComponent<AudioSource>().PlayOneShot(openSound);
        }
        else if (doorAction == "shut")
        {
            GetComponent<AudioSource>().PlayOneShot(shutSound);
        }
    }

    /// <summary>
    /// Activates door toggling between closed and open.
    /// </summary>
    public override void Activate()
    {
        if (locked == false)
        {
            Animator targetAnim = GetComponent<Animator>();
            bool toggle = targetAnim.GetBool("DoorOpen");

            if (toggle == true)
            {
                PlayDoorSound("shut");
            }
            else 
            {
                PlayDoorSound("open");
            }

            targetAnim.SetBool("DoorOpen", !toggle);
        }
        else if (locked == true)
        {
            PlayDoorSound("locked");
        }
    }

    /// <summary>
    /// Opens door by triggering the animation if door is not locked and if the door is closed.
    /// </summary>
    public void OpenDoor()
    {
        Animator targetAnim = GetComponent<Animator>();

        if (locked == false && targetAnim.GetBool("DoorOpen")  == false)
        {
            targetAnim.SetBool("DoorOpen", true);
            PlayDoorSound("open");
        }
    }
}
