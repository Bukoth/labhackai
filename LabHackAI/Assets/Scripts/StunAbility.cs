﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StunAbility : MonoBehaviour
{
    
    public Image stunBar;
    public float cooldownTime = 15;
    private float timer = 0;

    void Start()
    {
        //Tazer recharge is full at the start of the game.
        HUDManager.instance.stunCooldown.fillAmount = 1;
    }

    void Update()
    {
        //If tazer is used initiate the cooldown/recharge timer.
        if(GameController.instance.stunUsed == true)
        {
            StunCooldown();
        }
    }

    /// <summary>
    /// Times the stun cooldown time.
    /// Increments the stun power until full 
    /// When full sets 'used' to false so it can be used again
    /// </summary>
    public void StunCooldown()
    {
        if (timer >= 0)
        {
            timer += Time.deltaTime;

            HUDManager.instance.stunCooldown.fillAmount += (1 / (cooldownTime))*Time.deltaTime;

            if (timer >= cooldownTime)
            {
                timer = 0;

                HUDManager.instance.stunCooldown.fillAmount = 1;
                GameController.instance.stunUsed = false;
            }
        }
    }
}

