﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective1 : MonoBehaviour
{
    public GameController objectiveStatus;

    private void OnTriggerEnter(Collider other)
    {
        //If the correct lab is reached update the objective HUD objective

        if (other.tag == "Player")
        {
            objectiveStatus.objective1Achieved = true;
            HUDManager.instance.updateObjectiveCursors();
        }
    }
}
